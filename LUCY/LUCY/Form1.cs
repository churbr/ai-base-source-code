﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * In order to use the System namespaces of Speech recognition and synthesis,
 * you first need to include it in your references. So, to do that, go to:
 * References -> Add References -> Assemblies -> System.Speech
 */

using System.Speech.Recognition;
using System.Speech.Synthesis;

namespace LUCY
{
    public partial class Main : Form
    {
        SpeechRecognitionEngine recEngine = new SpeechRecognitionEngine();
        SpeechSynthesizer synthesizer = new SpeechSynthesizer();

        // ------------------------------------------------------------------------------------

        public Main()
        {
            InitializeComponent();
        }

        // ------------------------------------------------------------------------------------

        private void Main_Load(object sender, EventArgs e)
        {
            Choices commands = new Choices();
            commands.Add(new String[] { "hi", "hello" });

            GrammarBuilder gBuilder = new GrammarBuilder();
            gBuilder.Append(commands);
            Grammar grammar = new Grammar(gBuilder);

            recEngine.LoadGrammarAsync(grammar);
            recEngine.SetInputToDefaultAudioDevice();
            recEngine.RecognizeAsync(RecognizeMode.Multiple);
            recEngine.SpeechRecognized += recEngine_SpeechRecognized;
        }

        // ------------------------------------------------------------------------------------

        void recEngine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            switch (e.Result.Text)
            {
                case "hi":
                    synthesizer.SpeakAsync("Hello bryan. How are you?");
                    break;

                case "hello":
                    synthesizer.SpeakAsync("Hi bryan. How are you?");
                    break;
                default:
                    synthesizer.SpeakAsync("Not recognized.");
                    break;
            }
        }

        // ------------------------------------------------------------------------------------
    }
}
