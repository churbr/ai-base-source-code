﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.IO;
using System.Globalization;

namespace AI
{
    public partial class Main : Form
    {
        Grammar ConstantsGrammar, QueryGrammar;

        SpeechRecognitionEngine speechMemory = new SpeechRecognitionEngine(new CultureInfo("en-US"));
        SpeechSynthesizer LUCY = new SpeechSynthesizer();

        int index = 0;
        string[] ConstantsList;
        string[] QueryList;
        string[] ReplyList;

        string ConstantsPath    = @"Constants.txt";
        string directoryName    = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE";
        string QueryPath        = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Query.txt";
        string ReplyPath        = @"C:\Users\" + Environment.UserName + "\\Documents\\KNOWLEDGE BASE\\Reply.txt";

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            // This creates directory "KNOWLEDGE BASE" and creates txt files if not exist
            if (!Directory.Exists(directoryName)) { Directory.CreateDirectory(directoryName); }
            if (!File.Exists(QueryPath)) { File.Create(QueryPath); }
            if (!File.Exists(ReplyPath)) { File.Create(ReplyPath); }

            // Get the contents of the files, line by line and store it in an array
            try
            {
                ConstantsList = File.ReadAllLines(ConstantsPath);
                QueryList = File.ReadAllLines(QueryPath);
                ReplyList = File.ReadAllLines(ReplyPath);
            }
            catch { }

            // ---------------------- Loading the grammar ----------------------
            try
            {
                ConstantsGrammar = new Grammar(new GrammarBuilder(new Choices(ConstantsList)));
                speechMemory.LoadGrammar(ConstantsGrammar);
            }
            catch { }

            try
            {
                QueryGrammar = new Grammar(new GrammarBuilder(new Choices(QueryList)));
                speechMemory.LoadGrammar(QueryGrammar);
            }
            catch { }

            // ------------------------ Setting the event handlers ------------------------

            speechMemory.SetInputToDefaultAudioDevice();
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(QUERY_SpeechRecognized);
            speechMemory.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(CONSTANTS_SpeechRecognized);
            speechMemory.RecognizeAsync(RecognizeMode.Multiple);
        }

        // --- Events ---

        void CONSTANTS_SpeechRecognized(object sender, SpeechRecognizedEventArgs defaultEvent)
        {
            String speech = defaultEvent.Result.Text;

            switch(speech) {
                case "whats your name":
                    LUCY.SpeakAsync("I am Lucy.");
                    break;

                case "what version":
                    LUCY.SpeakAsync("Version 2.");
                    break;

                case "date of development":
                    LUCY.SpeakAsync("September 25, 2016");
                    break;
            }
        }

        // ---

        void QUERY_SpeechRecognized(object sender, SpeechRecognizedEventArgs defaultEvent)
        {
            String speech = defaultEvent.Result.Text;
            index = 0;

            try
            {
                foreach (string line in QueryList)
                {
                    if (line == speech)
                    {
                        LUCY.SpeakAsync(ReplyList[index]);
                    }
                    index++;
                }
            }
            catch { }
        }

        // ---


    }
}